export default { 
    data() {
        return {
            form: {
                body: '',
                media: [],
            },

            media: {
                images: [],

            },

            mediaTypes: {}
        }
    },

    methods: {
        async createPost() {
            if(this.media.images.length) {
                let media = await this.uploadMedia();
                this.form.media = media.data.data.map(list => list.id);
            }

            await this.submitPost();

            this.form.body = '';
            this.form.media = [];
            this.media.images = [];
        },

        async uploadMedia() {
            return await axios.post('/api/media', this.buildMediaForm(), {
                headers: {
                    'Content-type': 'multipart/form-data'
                }
            });
        },

        buildMediaForm() {
            let form = new FormData();

            if(this.media.images.length) {
                // get the index of the images so we can append a piece of data from the form
                this.media.images.forEach((image, index) => {
                    form.append(`media[${index}]`, image);
                })
            }

            return form;
        },

        removeImage(image) {
            this.media.images = this.media.images.filter((i) => {
                return image !== i;
            });
        },

        async getMediaTypes() {
            let response = await axios.get('/api/media/types');
            this.mediaTypes = response.data.data;
        },

        handleMediaSelected(files) {
            Array.from(files).slice(0, 4).forEach((file) => {
                if(this.mediaTypes.image.includes(file.type)) {
                    this.media.images.push(file);
                }
            })
        }
    },

    mounted() {
        this.getMediaTypes();
    },
}