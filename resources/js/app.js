import Vue from 'vue';
import Vuex from 'vuex';
import VueObserveVisibility from 'vue-observe-visibility';
import timeline from './store/timeline';
import likes from './store/likes';
import reposts from './store/reposts';
import notifications from './store/notifications';
import conversation from './store/conversation';
import profile from './store/profile';
import VModal from 'vue-js-modal';

require('./bootstrap');
Vue.prototype.$user = window.User;

Vue.use(Vuex);
Vue.use(VueObserveVisibility);

Vue.use(VModal, {
    dynamic: true,
    injectModalsContainer: true,
    DynamicDefaults: {
        pivotY: 0.1,
        height: 500,
        classes: 'bg-white p-1',
    }
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


const store = new Vuex.Store({
    modules: {
        timeline,
        likes,
        reposts,
        notifications,
        conversation,
        profile
    }
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store
});
