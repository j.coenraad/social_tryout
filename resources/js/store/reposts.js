import { without } from 'lodash';
export default {
    namespaced: true,

    state: {
        reposts: [],
    },

    getters: {
        reposts(state) {
            return state.reposts;
        }
    },

    actions: {
        sharePost({ commit }, post) {
            axios.post(`/api/posts/${post.id}/reposts`).then(response => {
                commit('PUSH_REPOST', response.data.data.original_post.id);

                commit('timeline/SET_REPOSTS', {
                    id: response.data.data.original_post.id,
                    count: response.data.data.original_post.repost_count
                }, { root: true });

                commit('conversation/SET_REPOSTS', {
                    id: response.data.data.original_post.id,
                    count: response.data.data.original_post.repost_count
                }, { root: true });

                commit('notifications/SET_REPOSTS', {
                    id: response.data.data.original_post.id,
                    count: response.data.data.original_post.repost_count
                }, { root: true });

                commit('profile/SET_REPOSTS', {
                    id: response.data.data.original_post.id,
                    count: response.data.data.original_post.repost_count
                }, { root: true });
                
                commit('timeline/CREATE_POST', response.data.data, {root: true });
            });
        },

        async unsharePost({ commit }, post){
            let response = await axios.delete(`/api/posts/${post.id}/reposts`);
            commit('timeline/REMOVE_POST', response.data.data.id, { root:true});
            commit('REMOVE_REPOST', response.data.data.original_post.id);

            commit('timeline/SET_REPOSTS', {
                id: response.data.data.original_post.id,
                count: response.data.data.original_post.repost_count
            }, { root: true });

            commit('conversation/SET_REPOSTS', {
                id: response.data.data.original_post.id,
                count: response.data.data.original_post.repost_count
            }, { root: true });

            commit('notifications/SET_REPOSTS', {
                id: response.data.data.original_post.id,
                count: response.data.data.original_post.repost_count
            }, { root: true });

            commit('profile/SET_REPOSTS', {
                id: response.data.data.original_post.id,
                count: response.data.data.original_post.repost_count
            }, { root: true });
        }
    },

    mutations: {
        PUSH_REPOSTS(state, data) {
            state.reposts.push(...data);
        },

        PUSH_REPOST(state, id) {
            state.reposts.push(id);
        },

        REMOVE_REPOST(state, id) {
            state.reposts = without(state.reposts, id);
        }
    }
}