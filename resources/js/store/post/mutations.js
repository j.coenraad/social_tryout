import { get } from 'lodash';

export default {
    PUSH_POSTS(state, data) {
        state.posts.push(...data);
    },

    CREATE_POST(state, data) {
        state.posts.unshift(data);
    },

    REMOVE_POST(state, id) {
        state.posts = state.posts.filter((post) => {
            return post.id !== id;
        });
    },

    SET_LIKES(state, { id, count }) {
        state.posts = state.posts.map((post) => {
            // first check if the original_post_id exists
            if(post.id === id) {
                post.likes_count = count;
            }

            // next check if the id matches the original_post_id
            if(get(post.original_post, 'id') === id) {
                post.original_post.likes_count = count;
            }

            return post;
        });
    },

    SET_REPOSTS(state, { id, count }) {
        state.posts = state.posts.map((post) => {
            if(post.id === id) {
                post.repost_count = count;
            }

            if(get(post.original_post, 'id') === id) {
                post.original_post.repost_count = count;
            }

            return post;
        });
    },

    SET_REPLIES(state, { id, count }) {
        state.posts = state.posts.map((post) => {
            if(post.id === id) {
                post.replies_count = count;
            }

            if(get(post.original_post, 'id') === id) {
                post.original_post.replies_count = count;
            }

            return post;
        });
    },
}