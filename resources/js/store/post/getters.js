export default {
    posts(state) {
        return state.posts;
    },

    post(state) {
        return id => state.posts.find(post => post.id === id);
    }
}