export default {
    async getPosts({ commit }, url) {
        let response = await axios.get(url);
        commit('PUSH_POSTS', response.data.data);
    
        commit('likes/PUSH_LIKES', response.data.meta.likes, { root:true });
        commit('reposts/PUSH_REPOSTS', response.data.meta.reposts, { root:true });

        return response;
    },

    async createPost({ commit }, post) {
        let response = await axios.post('/api/posts', post);
        commit('CREATE_POST', response.data.data);
    },

    async quotePost({ commit }, { post, data }) {
        let response = await axios.post(`/api/posts/${post.id}/quotes`, data);
        commit('reposts/PUSH_REPOST', response.data.data.original_post.id, {root: true});
        commit('SET_REPOSTS', {
            id: response.data.data.original_post.id,
            count: response.data.data.original_post.repost_count,
        });

        commit('CREATE_POST', response.data.data);
    },

    async replyToPost({ commit} , { post, data}) {
        let response = await axios.post(`/api/posts/${post.id}/replies`, data);

        commit('SET_REPLIES', {
            id: response.data.data.id,
            count: response.data.data.replies_count
        });

        commit('conversation/SET_REPLIES', {
            id: response.data.data.id,
            count: response.data.data.replies_count
        }, { root: true });

        commit('notifications/SET_REPLIES', {
            id: response.data.data.id,
            count: response.data.data.replies_count
        }, { root: true });

        commit('profile/SET_REPLIES', {
            id: response.data.data.id,
            count: response.data.data.replies_count
        }, { root: true });
    }
}