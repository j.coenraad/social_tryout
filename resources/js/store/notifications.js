import getters from './post/getters';
import mutations from './post/mutations';
import actions from './post/actions';

export default {
    namespaced: true,

    state: {
        notifications: [],
        posts: [],
    },

    getters: {
        ...getters,

        notifications(state) {
            return state.notifications;
        },

        postIdsFromNotifications(state) {
            // returns all of the posts related to all the notifications
            return state.notifications.map(notification => notification.data.post.id);
        }
    },

    actions: {
        ...actions,

        async getNotifications({ commit, dispatch, getters }, url) {
            let response = await axios.get(url);
            commit('PUSH_NOTIFICATIONS', response.data.data);
            dispatch('getPosts', `/api/posts?ids=${getters.postIdsFromNotifications.join(',')}`);
            // example url api/posts?ids=24,31

            return response;
        }
    },

    mutations: {
        ...mutations, 
        
        PUSH_NOTIFICATIONS(state, data) {
            state.notifications.push(...data);
        }
    }
}