import { without } from 'lodash';

export default {
    namespaced: true,

    state: {
        likes: [],
    },

    getters: {
        likes(state) {
            return state.likes;
        }
    },

    actions: {
        async likePost({ commit }, post) {
            let response = await axios.post(`/api/posts/${post.id}/likes`);
            commit('PUSH_LIKE', post.id);

            commit('timeline/SET_LIKES', {
                id: response.data.data.id,
                count: response.data.data.likes_count
            }, {root: true});

            commit('conversation/SET_LIKES', {
                id: response.data.data.id,
                count: response.data.data.likes_count
            }, {root: true});

            commit('notifications/SET_LIKES', {
                id: response.data.data.id,
                count: response.data.data.likes_count
            }, {root: true});

            commit('profile/SET_LIKES', {
                id: response.data.data.id,
                count: response.data.data.likes_count
            }, {root: true});
        },

        async unlikePost({ commit }, post) {
            let response = await axios.delete(`/api/posts/${post.id}/likes`);
            commit('REMOVE_LIKE', post.id);

            commit('timeline/SET_LIKES', {
                id: response.data.data.id,
                count: response.data.data.likes_count
            }, { root: true});

            commit('conversation/SET_LIKES', {
                id: response.data.data.id,
                count: response.data.data.likes_count
            }, {root: true});

            commit('notifications/SET_LIKES', {
                id: response.data.data.id,
                count: response.data.data.likes_count
            }, {root: true});

            commit('profile/SET_LIKES', {
                id: response.data.data.id,
                count: response.data.data.likes_count
            }, {root: true});
        }
    },

    mutations: {
        PUSH_LIKES(state, data) {
            state.likes.push(...data);
        },

        PUSH_LIKE(state, id) {
            state.likes.push(id);
        },

        REMOVE_LIKE(state, id) {
            state.likes = without(state.likes, id);
        }
    }
}