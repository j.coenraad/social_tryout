import actions from './post/actions';
import mutations from './post/mutations';

export default {
    namespaced: true,

    state: {
        posts: [],
    },

    getters: {
        post(state) {
            return id => state.posts.find(post => post.id == id);
        },

        parents(state) {
            // filter the posts where the post id does not match the id and return the oldest first
            return id => state.posts.filter(post => {
                return post.id != id && !post.parent_ids.includes(parseInt(id))
            }).sort((a, b) => a.created_at - b.created_at);
        },

        replies(state) {
            return id => state.posts.filter(post => post.parent_id == id)
                .sort((a, b) => a.created_at - b.created_at);
        }
    },

    actions: {
        ...actions,
    },

    mutations: {
        ...mutations,
    }
}