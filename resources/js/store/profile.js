import getters from './post/getters';
import actions from './post/actions';
import mutations from './post/mutations';

export default{
    namespaced: true,

    state: {
        profile: null,
        profiles: [],
        posts: [],
        followers: [],
        status: null,
    },

    getters: {
        ...getters, 

        profile(state) {
            return state.profile;
        },

        profiles(state) {
            return state.profiles;
        },

        followers(state) {
            return state.followers;
        },

        status(state) {
            return state.status;
        }
    },

    actions: {
        ...actions, 

        async getProfile({ commit }, username) {
            let response = await axios.get(`/api/profiles/${username}`);
            commit('SET_PROFILE', response.data.data);
            commit('SET_FOLLOWERS', response.data.meta.followers);
            commit('SET_PROFILE_STATUS', 'success');

            return response;
        },

        async getProfiles({ commit }, url) {
            let response = await axios.get(url);
            commit('PUSH_PROFILES', response.data.data);
            commit('SET_FOLLOWERS', response.data.meta.followers);

            return response;
        },

        async updateProfile({ commit}, {username, profileData}) {
            let response = await axios.put(`/api/profiles/${username}/edit`, profileData);
            commit('SET_PROFILE', response.data.data);
            
            return response;
        },
        
        async followProfile({ commit }, username) {
            let response = await axios.post(`/api/profiles/${username}/follow`);
            commit('SET_PROFILE', response.data.data);
            commit('SET_FOLLOWERS', response.data.meta.followers);
        },

        async unfollowProfile({ commit }, username) {
            let response = await axios.delete(`/api/profiles/${username}/unfollow`);
            commit('SET_PROFILE', response.data.data);
            commit('SET_FOLLOWERS', response.data.meta.followers);
        }
    },

    mutations : {
        ...mutations,

        SET_PROFILE(state, data) {
            state.profile = data;
        },

        PUSH_PROFILES(state, data) {
            state.profiles.push(...data)
        },

        SET_FOLLOWERS(state, data) {
            state.followers = data;
        },

        SET_PROFILE_STATUS(state, data) {
            state.status = data;
        },
    }
}