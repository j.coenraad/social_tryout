<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body class="font-sans bg-white text-gray-800 antialiased">
    <div id="app">
        <main class="container">
            <div class="flex">

                {{-- Nav --}}
                <div class="w-4/12 h-12 pl-32 py-4 h-auto">
                    <div class="fixed">
                        <svg viewBox="0 0 24 24" class="h-12 w-12 text-blue-500" fill="currentColor">
                            <g>
                                <path d="M23.643 4.937c-.835.37-1.732.62-2.675.733.962-.576 1.7-1.49 2.048-2.578-.9.534-1.897.922-2.958 1.13-.85-.904-2.06-1.47-3.4-1.47-2.572 0-4.658 2.086-4.658 4.66 0 .364.042.718.12 1.06-3.873-.195-7.304-2.05-9.602-4.868-.4.69-.63 1.49-.63 2.342 0 1.616.823 3.043 2.072 3.878-.764-.025-1.482-.234-2.11-.583v.06c0 2.257 1.605 4.14 3.737 4.568-.392.106-.803.162-1.227.162-.3 0-.593-.028-.877-.082.593 1.85 2.313 3.198 4.352 3.234-1.595 1.25-3.604 1.995-5.786 1.995-.376 0-.747-.022-1.112-.065 2.062 1.323 4.51 2.093 7.14 2.093 8.57 0 13.255-7.098 13.255-13.254 0-.2-.005-.402-.014-.602.91-.658 1.7-1.477 2.323-2.41z">
                                </path>
                            </g>
                        </svg>
                        
                        <nav class="mt-5 px-2">
                            <a href="#" class="group flex items-center px-2 py-2 text-base leading-6 font-semibold rounded-full">
                                Home
                            </a>
                            <a href="" class="group flex items-center px-2 py-2 text-base leading-6 font-semibold rounded-full">
                                Notifications
                            </a>
                            <a href="" class="group flex items-center px-2 py-2 text-base leading-6 font-semibold rounded-full">
                                Profile
                            </a>
                        </nav>
                    </div>
                </div>
                {{-- End nav --}}

                <div class="w-7/12 border border-gray-800 border-t-0">
                    <div class="flex">
                        <div class="flex-1 m-2">
                            <h2 class="px-4 py-2 text-xl font-semibold text-gray-800">
                                Home
                            </h2>
                        </div>
                        <div class="flex-1 px-4 py-2 m-2">
                            <a href="" class=" text-2xl font-medium rounded-full text-gray-800 hover:bg-blue-800 hover:text-blue-300 float-right">
                                <svg class="m-2 h-6 w-6" fill="currentColor" viewBox="0 0 24 24"><g><path d="M22.772 10.506l-5.618-2.192-2.16-6.5c-.102-.307-.39-.514-.712-.514s-.61.207-.712.513l-2.16 6.5-5.62 2.192c-.287.112-.477.39-.477.7s.19.585.478.698l5.62 2.192 2.16 6.5c.102.306.39.513.712.513s.61-.207.712-.513l2.16-6.5 5.62-2.192c.287-.112.477-.39.477-.7s-.19-.585-.478-.697zm-6.49 2.32c-.208.08-.37.25-.44.46l-1.56 4.695-1.56-4.693c-.07-.21-.23-.38-.438-.462l-4.155-1.62 4.154-1.622c.208-.08.37-.25.44-.462l1.56-4.693 1.56 4.694c.07.212.23.382.438.463l4.155 1.62-4.155 1.622zM6.663 3.812h-1.88V2.05c0-.414-.337-.75-.75-.75s-.75.336-.75.75v1.762H1.5c-.414 0-.75.336-.75.75s.336.75.75.75h1.782v1.762c0 .414.336.75.75.75s.75-.336.75-.75V5.312h1.88c.415 0 .75-.336.75-.75s-.335-.75-.75-.75zm2.535 15.622h-1.1v-1.016c0-.414-.335-.75-.75-.75s-.75.336-.75.75v1.016H5.57c-.414 0-.75.336-.75.75s.336.75.75.75H6.6v1.016c0 .414.335.75.75.75s.75-.336.75-.75v-1.016h1.098c.414 0 .75-.336.75-.75s-.336-.75-.75-.75z"></path></g>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <hr class="border-gray-600" />

                    <div class="w-full h-48 bg-cover bg-norepeat bg-center" style="background-image: url(https://images.pexels.com/photos/346529/pexels-photo-346529.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940);">
                        <img src="https://images.pexels.com/photos/346529/pexels-photo-346529.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="" class="h-48 w-full ">
                    </div>
                    <div class="p-4">
                        <div class="relative flex w-full">
                            <div class="flex flex-1">
                                <div class="-mt-20">
                                    <div class="flex flex-shrink-0 h-32 w-32 rounded-full relative">
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAYFBMVEXs7/WOl6bv8viQmaiJkqLl6e+LlKTr7vQAAAChqbaHkKCLjZDMztP09/3g5OuYoK69w83Kz9ja3eKrsr7i5ezT2ODGy9W0u8Y+P0Gxt8OcpLGWnq2yucSkrLi7wczIztf60yT5AAAGCUlEQVR4nO2d3XbqIBBGkwkJJmp+jbax2vd/y0O0Z1mrPYYwST447Jte1r0GAgwDBKvVar2RgYvIzVrZBau3ektL/5aJoG39pgxrV/16qF4F6+3Sv2JStutg43IIVRA3gZsfmRuu+3k8Ho/H4/F4PB6Px+PxeDwej8fj8Xg8Hs8S0E+W/kGMKBsZ5+3+/N6kXVd0Xdq8n/dtHksnPIniumy6JBMi6QnD8PJXiCzpmrKO7ZZUelVzVG7hc5TnsanslSRqm+RXu5tl0rRWOpKsiuyV3pdkVlTSOkeqCjFI74ooKrsUaZfq+F0c051FjlS+7H6PJKK0RZHik24Av8J4iq1wpLzQD+BXGIvcAkWqo7GCSjHCr3alerzfxRFdkXIjvx7whiqPZiFUQTxCV4TSyVRQKZ6Ag6jGQWNBNWgAj4u5eQR7knxpkd/YpkyGKej5Aao42miPQJ2GF0yCYVgsrfIUvhCiBpGYemFPkiIa5nwhVEEE/JzSmS+EKohnvCDSkVEwDI94hnnGapjBNVPac3ZD1RH3aEGkhrMbqo7YwBlGrIJhGKEZxrwhVEGMl1b6gWHy4olhvbTSPdTyfmjUp6bFaqack9IvQ7CpqTd0wND5fsiVorkBl6yJmQXxxkPiNgzBGmlAfEmaKwWcIUO2+zt4mW86MBse4AzdXx8yD4hww6EaLpizGLulhR4g5n4IF8KAOlbDDtCQNVGDl6bpd0dZDQF3SXnXT2hrpwvO71sEMWc+MUJbWVxIGQ3TpWWeQJJzuOgAK2pjg3K2RxK8Zsq5A3xRRNsFdj8Txb0AxlsCsw4VV8B6IutwfwVr0OfvhmgdkT+pjzY1/Q8MnW+l/NsWcBsX7o8WjJWXfwFL63OX0+BlarhT3oBJb/d3SKljXj3BJUypZc7qY42GPbzrJ7S10wXZMVayd5CHn+Q5YTpRkpwhBfvjsR8s554+gA/LUstwdg3vG/Md84p9xCr9O4y3gjOwCfcDxocSAI8h3GM6BUebcD9iOgWHm3A/Upt1xAysvPsJ0my1H4GO9d8w64j43dA0sYiVQvyFnUkzjfBKoR4xaaY2NFKz7DBYFvg3DMqF4VIXzxmf0ABMXTxnbEIDMnXxnLGX1KAlEH+HPsd8bMSnNSHs69r1FQVc7fo/0e+KFnXCK1IzB55g5g//AWlO3iKb7hS8ojcDt2Qyc49OpRtcFdsgdIJoxaLpETl88pbZ9pm5opFYBNuzH8rwjmhnN9QZ9a0b7b/4Dwzdb6XDi/ctWdv/ROPKGrjLaIZBw8dDYafhTmPEtyFN+oBOVtHKibfWgUTE44avcX9toXcXCPru/RM0V8AWLp/0jnpZ2Ex172aHv4v9Ad09Njv21b6hv8Vm25A4onAIvlToDuez+uOqFSwaMcbWYILXXd4YX2RqiSJVBrUYNjRUOhtVDKHXzwYUaz9l9UMxBS7yDvoWanyzQhIBt1TaNSy1+g3oXiLJku28RQl48QfJfcR4ZibagzmS/NR6DHCAY/EJ5EhBeZzgLPexDCAciXaHhN/v4pgcdou/30myPY1463AoiTi1SzZWorwsJvS7OhZlvkwg1X+t0oma5z0iSatgdkkK6o/XL+Fyof7TRz3nZ0d9XMpuNr2/kl0512eHqDqFc7TOn4jwVE3vqD4u52jm8N1IRHSY9rPTjw0D32meTDKbcPygePKxYZCjGj+mWEOq5tks0vueIcKGu7FS0HYLN897kqxrGYcPktXcg8Nr1PDB9SA7BXvmlREXotgzxJGIc2XLjVopG/ZHNfcE9usRUWUQRzV7AW2f3xHF6JkO5SNfgZ8bcRr10jXJA9z38zcScdD/rFLLemvu1CSR5qYOyXc7GugN8a4TRsqNn0ifn+Q4vDdOcZPlHAzeROZ5IX0JBr7Kbq/gQEWqeC9em5dsQEPdLf0jDXlZbcz9btPcvH4nSvJfRDovxatDYhNcJjsvL6+u9YbweENviI839Ib4eENviI839Ib4/I+G0nFDGWzIaUPaBOut04bbdbC6P9romCHVq2D1Vm/JUUPa1m/KcLVab6SThnKzVnZ/ABdeboTJyBh6AAAAAElFTkSuQmCC"
                                            alt="" 
                                            class="h-32 w-32 rounded-full relative border-4 border-gray-500">
                                    </div>
                                </div> 
                            </div>
                            <div class="flex flex-col justify-end pr-4">
                                <button class="btn-primary ">
                                    Edit Profile
                                </button>
                            </div>
                        </div>

                        <div class="space-y-1 justify-center w-full mt-3 ml-3">
                            <div>
                                <h2 class="text-xl leading-6 font-bold">Name of user</h2>
                                <p class="text-sm leading-5 font-medium text-gray-500">@username</p>
                            </div>

                            <div class="mt-3">
                                <p class="leading-tight mb-2">
                                    Some description
                                </p>
                                <div class="text-gray-600">
                                    <span class="flex mr-2">
                                        <svg 
                                            xmlns="http://www.w3.org/2000/svg" 
                                            fill="none" 
                                            viewBox="0 0 24 24" 
                                            stroke="currentColor"
                                            class="fill-current text-gray-600 w-5 mr-2">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                                        </svg>
                                        <span class="leading-5 ml-1">Joined December, 2019</span>
                                    </span>
                                </div>
                            </div>

                            <div class="pt-3 flex justify-start items-center w-full divide-x divide-gray-800 divide-solid">
                                <div class="text-center pr-3">
                                    <span class="font-bold">520</span>
                                    <span class="text-gray-600">following</span>
                                </div>
                                <div class="text-center px-3">
                                    <span class="font-bold">23,4 m </span>
                                    <span class="text-gray-600"> followers</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="border-gray-600" />
                    {{-- User posts section --}}
                    @for($i = 0; $i < 5; $i++)
                        <div class="w-full inline-block p-4 border-b border-gray-600">
                            <div class="flex w-full">
                                <a href="#" class="flex flex-shrink-0 block">
                                    <div>
                                        <img class="inline-block h-10 w-10 rounded-full mr-3" 
                                            src="https://pbs.twimg.com/profile_images/1121328878142853120/e-rpjoJi_bigger.png" 
                                            alt="" />
                                    </div>
                                </a>
                                <div class="flex-grow">
                                    <p class="text-gray-700 font-bold">
                                        <a href="#">Name of user</a>
                                        <span class="text-sm font-medium text-gray-500 hover:text-gray-400 transition ease-in-out duration-150">
                                            @username
                                        </span>
                                    </p>
                        
                                    <p class="whitespace pre-wrap">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce turpis metus, maximus consequat mollis vitae, ornare elementum odio. Nam ut arcu tempus risus sollicitudin sollicitudin. Nulla ultrices auctor tincidunt. Praesent at nisi venenatis dolor.
                                    </p>
    
                                    {{-- Action button group --}}
                                    <ul class="flex mt-4">
                                        {{-- Reply action button --}}
                                        <li class="w-3/12">
                                            <a href="#" class="flex items-center text-base">
                                                <svg xmlns="http://www.w3.org/2000/svg" 
                                                    viewBox="0 0 24 24" 
                                                    width="24" 
                                                    height="24" 
                                                    class="fill-current text-gray-600 w-5 mr-2"
                                                >
                                                    <path d="M2 15V5c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v15a1 1 0 0 1-1.7.7L16.58 17H4a2 2 0 0 1-2-2zM20 5H4v10h13a1 1 0 0 1 .7.3l2.3 2.29V5z"/>
                                                </svg>
                                                <span class="text-gray-600">0</span>
                                            </a>
                                        </li>
                                        {{-- End reply action button --}}
    
                                        {{-- Share action button --}}
                                        <li class="w-3/12">
                                            <a href="#" class="flex items-center text-base">
                                                <svg 
                                                    xmlns="http://www.w3.org/2000/svg" 
                                                    viewBox="0 0 24 24"  
                                                    width="24"  
                                                    height="24"  
                                                    class="fill-current text-gray-600 w-5 mr-2" 
                                                >
                                                    <path class="heroicon-ui" d="M5.41 16H18a2 2 0 0 0 2-2 1 1 0 0 1 2 0 4 4 0 0 1-4 4H5.41l2.3 2.3a1 1 0 0 1-1.42 1.4l-4-4a1 1 0 0 1 0-1.4l4-4a1 1 0 1 1 1.42 1.4L5.4 16zM6 8a2 2 0 0 0-2 2 1 1 0 0 1-2 0 4 4 0 0 1 4-4h12.59l-2.3-2.3a1 1 0 1 1 1.42-1.4l4 4a1 1 0 0 1 0 1.4l-4 4a1 1 0 0 1-1.42-1.4L18.6 8H6z"/>
                                                </svg>
                                                <span class="text-gray-600">
                                                    0
                                                </span>
                                            </a>
                                        </li>
                                        {{-- End share action --}}
    
                                        {{-- Like action button --}}
                                        <li class="w-3/12">
                                            <a href="#" class="flex items-center text-base">
                                                <svg xmlns="http://www.w3.org/2000/svg" 
                                                    viewBox="0 0 24 24" 
                                                    width="24" 
                                                    height="24" 
                                                    class="fill-current text-gray-600 w-5 mr-2"
                                                >
                                                    <path class="heroicon-ui" d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z"/>
                                                </svg>
                                                <span class="text-gray-600"> 0 </span>
                                            </a>
                                        </li>
                                        {{-- End like action button --}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>

            </div>
        </main>
    </div>
</body>
</html>