@extends('layouts.guest')

@section('content')
    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
        <div class="flex justify-center font-bold text-lg text-gray-700uppercase">Login</div>
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="mt-4">
                <label for="email" class="block font-medium text-sm text-gray-700">E-mail</label>
                <input 
                    id="email" 
                    name="email"
                    type="email"
                    class="form-input @error('email') !border-red-500 @enderror"
                    placeholder="E-mail"
                    value="{{ old('email') }}"
                    autofocus
                    required
                >

                @error('email')
                    <span class="mt-3 text-sm text-red-600" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="mt-4">
                <label for="password" class="block font-medium text-sm text-gray-700">Password</label> 
                <input 
                    id="password" 
                    type="password" 
                    name="password" 
                    class="form-input @error('password') !border-red-500 @enderror">

                @error('password')
                <span class="mt-3 text-sm text-red-600" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <input id="remember_me" type="checkbox" class="form-checkbox" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <button type="submit" class="ml-4 px-4 py-2 bg-gray-800 rounded-md font-semibold text-xs text-white uppercase tracking-widest focus:outline-none focus:border-gray-900 focus:shadow-outline-gray">
                    Login
                </button>
            </div>
        </form>
    </div>
@endsection
