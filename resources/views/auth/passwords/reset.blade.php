@extends('layouts.guest')

@section('content')
<div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
    <div class="flex justify-center font-bold text-lg text-gray-700 uppercase">Reset your password</div>
    <form method="POST" action="{{ route('password.update') }}">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">

        <div class="mt-4">
            <label for="email" class="block font-medium text-sm text-gray-700">E-mail</label>
            <input 
                id="email" 
                name="email" 
                type="email" 
                class="form-input @error('email') !border-red-500 @enderror" 
                required
            >

            @error('email')
                <span class="mt-3 text-sm text-red-600" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="mt-4">
            <label for="password" class="block font-medium text-sm text-gray-700">Password</label> 
            <input 
                id="password" 
                name="password" 
                type="password" 
                class="form-input @error('password') !border-red-500 @enderror" 
                required
            >

            @error('password')
                <span class="mt-3 text-sm text-red-600" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="mt-4">
            <div class="mt-4">
                <label for="password_confirmation" class="block font-medium text-sm text-gray-700">Confirm Password</label> 
                <input 
                    id="password_confirmation" 
                    name="password_confirmation" 
                    type="password" 
                    class="form-input" 
                    required
                >
            </div>
        </div>

        <div class="flex items-center justify-end mt-4">
            <button type="submit" class="ml-4 px-4 py-2 bg-gray-800 rounded-md font-semibold text-xs text-white uppercase tracking-widest focus:outline-none focus:border-gray-900 focus:shadow-outline-gray">
               Reset Password 
            </button>
        </div>
    </form>
</div>
@endsection
