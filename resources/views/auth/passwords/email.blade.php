@extends('layouts.guest')

@section('content')
<div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
    <div class="flex justify-center font-bold text-lg text-gray-700 uppercase">Forgot your password?</div>
    <div class="mb-4 text-sm text-gray-700">
        No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.
    </div>

    @if (session('status'))
        <div class="mb-4 font-medium text-sm text-green-600">
            {{ session('status') }}
        </div>
    @endif

    @error('email')
        <span class="mt-3 text-sm text-red-600" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror

    <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="block">
            <label for="email" class="block font-medium text-sm text-gray-700">E-mail</label>
            <input 
                id="email" 
                name="email" 
                type="email" 
                class="form-input">
        </div>

        <div class="flex items-center justify-end mt-4">
            <button type="submit" class="ml-4 px-4 py-2 bg-gray-800 rounded-md font-semibold text-xs text-white uppercase tracking-widest focus:outline-none focus:border-gray-900 focus:shadow-outline-gray">
               Email password reset link
            </button>
        </div>
    </form>
</div>
@endsection
