@extends('layouts.guest')

@section('content')
        <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
            <div class="flex justify-center font-bold text-lg text-gray-700 uppercase">Register</div>

            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="mt-4">
                    <label for="name" class="block font-medium text-sm text-gray-700">Name</label>
                    <input 
                        id="name" 
                        name="name" 
                        type="text" 
                        class="form-input @error('name') !border-red-500 @enderror"
                        placeholder="Your Name"
                        value="{{ old('name') }}" 
                        autofocus
                        required
                    >

                    @error('name')
                        <span class="mt-3 text-sm text-red-600" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
        
                <div class="mt-4">
                    <label for="username" class="block font-medium text-sm text-gray-700">Username</label>
                    <input 
                        id="username" 
                        name="username" 
                        type="text" 
                        class="form-input @error('username') !border-red-500 @enderror"
                        placeholder='username123'
                        value="{{ old('username') }}"
                    >

                    @error('username')
                        <span class="mt-3 text-sm text-red-600" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
        
                <div class="mt-4">
                    <label for="email" class="block font-medium text-sm text-gray-700">E-mail</label>
                    <input 
                        id="email" 
                        type="email" 
                        name="email" 
                        class="form-input @error('email') !border-red-500 @enderror"
                        placeholder="your@email.com"
                        value="{{ old('email') }}"
                        required
                    >

                    @error('email')
                        <span class="mt-3 text-sm text-red-600" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
        
                <div class="mt-4">
                    <label for="password" class="block font-medium text-sm text-gray-700">Password</label> 
                    <input 
                        id="password" 
                        name="password" 
                        type="password" 
                        class="form-input @error('password') !border-red-500 @enderror" 
                        required
                    >

                    @error('password')
                        <span class="mt-3 text-sm text-red-600" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
        
                <div class="mt-4">
                    <label for="password" class="block font-medium text-sm text-gray-700">Password confirmation</label>
                    <input 
                        id="password-confirm" 
                        name="password_confirmation" 
                        type="password" 
                        class="form-input" 
                        required
                    >
                </div>
        
                <div class="flex items-center justify-end mt-4">
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">Already registered?</a>
                    <button type="submit" class="ml-4 px-4 py-2 bg-gray-800 rounded-md font-semibold text-xs text-white uppercase tracking-widest focus:outline-none focus:border-gray-900 focus:shadow-outline-gray">
                        Register
                    </button>
                </div>
            </form>
    </div>
@endsection
