@extends('layouts.guest')

@section('content')
<div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
    <div class="flex justify-center font-bold text-lg text-gray-700 uppercase">Verify your email address</div>

    @if (session('resent'))
        <div class="mb-4 font-medium text-sm text-green-600">
            {{ __('A fresh verification link has been sent to your email address.') }}
        </div>
    @endif

    <div class="mb-4 text-sm text-gray-600">
        Before proceeding, please check your email for a verification link.
        If you did not receive the email, we will gladly send you another.
    </div>

    <div class="mt-4 flex items-center justify-between">
        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
            @csrf
            <button type="submit" class="ml-4 px-4 py-2 bg-gray-800 rounded-md font-semibold text-xs text-white uppercase tracking-widest focus:outline-none focus:border-gray-900 focus:shadow-outline-gray">
                Resend verification email
            </button>
        </form>
    </div>
</div>
@endsection
