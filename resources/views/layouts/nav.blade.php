<div class="w-4/12 h-12 pl-32 py-4 h-auto">
    <div class="fixed">

        @auth
            <nav class="mt-5 px-2">
                <a href="{{ route('home') }}" class="group flex items-center px-2 py-2 text-base leading-6 font-bold rounded-full hover:bg-blue-200 hover:text-blue-600">
                    <svg class="mr-4 h-6 w-6 " stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l9-9 9 9M5 10v10a1 1 0 001 1h3a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1h3a1 1 0 001-1V10M9 21h6"/>
                    </svg>
                    Home
                </a>
                <a href="{{ route('notifications')}}" class="group flex items-center px-2 py-2 text-base leading-6 font-semibold rounded-full">
                    <svg class="mr-4 h-6 w-6" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" stroke="currentColor" viewBox="0 0 24 24">
                        <path d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"></path>
                    </svg>
                    Notifications
                </a>
                <a href="{{ route('profile', auth()->user()->username)}}" class="group flex items-center px-2 py-2 text-base leading-6 font-semibold rounded-full">
                    <svg class="mr-4 h-6 w-6" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" stroke="currentColor" viewBox="0 0 24 24">
                        <path d="M8 12h.01M12 12h.01M16 12h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                    </svg>
                    Profile
                </a>
                <hr/>
                <div class="mt-2">
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <button class="group flex items-center px-2 py-2 text-base leading-6 font-semibold rounded-full hover:bg-blue-200 focus:outline-none">Logout</button>
                    </form>
                </div>
            </nav>
        @endauth
    </div>
</div>