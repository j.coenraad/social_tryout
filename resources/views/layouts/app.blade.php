<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <script>
        window.User = {
            id: {{ optional(auth()->user())->id }}
        }
    </script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="font-sans bg-white text-gray-800 antialiased">
    <div id="app">
        <main class="container">
            <div class="flex">
                @include('layouts.nav')
                @yield('content')
            </div>
            
            <modals-container />
        </main>
    </div>
</body>
</html>
