@extends('layouts.app')

@section('content')
<div class="w-7/12 border border-gray-400 border-t-0">
    <div class="sticky top-0 bg-white border-b border-gray-400">
        <div class="flex-1 m-2">
            <a href="{{ route('profile', $user) }}">
                <h2 class="px-4 py-1 text-xl font-semibold text-gray-800">
                    {{ $user->name }}
                </h2>
            </a>
            <p class="px-4 text-sm font-normal text-gray-600">{{ '@' . $user->name }}</p>
        </div>
    </div>
    <hr class="border-gray-400"/>
    <app-profile-followers username="{{ $user->username }}" />
</div>
@endsection