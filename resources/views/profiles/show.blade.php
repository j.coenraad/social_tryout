@extends('layouts.app')

@section('content')
    <div class="w-7/12 border border-gray-400 border-t-0">
        <div class="sticky top-0 bg-white border-b border-gray-400">
            <div class="flex-1 m-2">
                <h2 class="px-4 py-1 text-xl font-semibold text-gray-800">
                    {{ $user->name }}
                </h2>
                <p class="px-4 text-sm font-normal text-gray-600">{{ $user->posts()->count() }} posts</p>
            </div>
        </div>
        <hr class="border-gray-400"/>
        <app-profile username="{{ $user->username }}" />
    </div>
@endsection
