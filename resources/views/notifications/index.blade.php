@extends('layouts.app')

@section('content')
    <div class="w-7/12 border border-gray-400 border-t-0">
        <div class="sticky top-0 bg-white border-b border-gray-400">
            <div class="flex-1 m-2">
                <h2 class="px-4 py-2 text-xl font-semibold">
                    Notifications
                </h2>
            </div>
        </div>
        <hr class="border-gray-400" />
        <app-notifications />
    </div>
@endsection