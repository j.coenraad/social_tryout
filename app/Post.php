<?php

namespace App;


use App\Posts\Entities\EntityExtractor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Post extends Model
{
    protected $fillable = ['user_id', 'body', 'type', 'original_post_id', 'parent_id'];

    public static function boot()
    {
        parent::boot();

        static::created(function (Post $post) {
            $post->entities()->createMany(
                (new EntityExtractor($post->body))->getAllEntities()
            );
        });
    }
 
    public function scopeParent(Builder $builder)
    {
        return $builder->whereNull('parent_id');
    }

    /**
     * append the parent of the post to the parents array
     * set the base to the parent of the post
     * and loop over it till the is no parent post
     * return an eloquent collection of the parents array
     */
    public function parents()
    {
        $base = $this;
        $parents = [];

        while($base->parentPost) {
            $parents[] = $base->parentPost;
            $base = $base->parentPost;
        }

        return collect($parents);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function originalPost()
    {
        return $this->hasOne(Post::class, 'id', 'original_post_id');
    }

    public function parentPost()
    {
        return $this->belongsTo(Post::class, 'parent_id');
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function reposts()
    {
        return $this->hasMany(Post::class, 'original_post_id');
    }

    public function sharedPost()
    {
        return $this->hasOne(Post::class, 'original_post_id', 'id');
    }

    public function media()
    {
        return $this->hasMany(PostMedia::class);
    }

    public function replies()
    {
        return $this->hasMany(Post::class, 'parent_id');
    }

    public function entities()
    {
        return $this->hasMany(Entity::class);
    }

    public function mentions()
    {
        return $this->hasMany(Entity::class)->whereType('mention');
    }
}
