<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, Followable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function booted()
    {
        static::created( function($user) {
            Profile::create(['user_id' => $user->id]);
        });
    }

    public function timeline()
    {
        $friends = $this->follows()->pluck('id');
        return Post::whereIn('user_id', $friends)
            ->orWhere('user_id', $this->id)
            ->parent()
            ->with(['user',
                'likes', 
                'reposts', 
                'replies',
                'entities',
                'media.baseMedia',
                'originalPost.user',
                'originalPost.likes', 
                'originalPost.reposts', 
                'originalPost.media.baseMedia'
            ])
            ->latest()
            ->paginate(10);
    }

    public function hasLiked($post)
    {
        // check if the given post id is in the list of likes
        return $this->likes->contains('post_id', $post->id);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function reposts()
    {
        return $this->hasMany(Post::class)->where('type', 'repost')->orWhere('type', 'quote');
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function getRouteKeyName()
    {
        return 'username';
    }
}
