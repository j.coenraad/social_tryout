<?php

namespace App;

trait Followable {

    public function follow(User $user)
    {
        return $this->follows()->save($user);
    }

    public function unfollow(User $user)
    {
        return $this->follows()->detach($user);
    }

    /**
     * Who is following the current user
     */
    public function followers()
    {
        return $this->belongsToMany(User::class, 'follows', 'following_user_id', 'user_id')->withTimestamps();
    }

    /**
     * who is the current user following
     */
    public function follows()
    {
        return $this->belongstoMany(User::class, 'follows', 'user_id', 'following_user_id')->withTimestamps();
    }
}