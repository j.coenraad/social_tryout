<?php

namespace App\Media;

use App\Media\MediaTypes;
use Spatie\MediaLibrary\Models\Media as BaseMedia;

class Media extends BaseMedia
{
    public function type()
    {
        return MediaTypes::type($this->mime_type);
    }
}