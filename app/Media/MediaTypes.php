<?php

namespace App\Media;

class MediaTypes
{
    public static $image = [
        'image/png',
        'image/jpg',
        'image/jpeg'
    ];

    public static function type($mime)
    {
        if(in_array($mime, self::$image)) {
            return 'image';
        }

        return null;
    }
}