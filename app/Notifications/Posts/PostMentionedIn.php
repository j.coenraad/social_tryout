<?php

namespace App\Notifications\Posts;

use App\Post;
use App\User;
use App\Http\Resources\UserResource;
use App\Http\Resources\PostResource;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\DatabaseNotificationChannel;
use Illuminate\Notifications\Messages\MailMessage;

class PostMentionedIn extends Notification
{
    use Queueable;

    protected $user, $post;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Post $post)
    {
        $this->user = $user;
        $this->post = $post;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            DatabaseNotificationChannel::class
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user' =>  new UserResource($this->user),
            'post' =>  new PostResource($this->post),
        ];
    }
}
