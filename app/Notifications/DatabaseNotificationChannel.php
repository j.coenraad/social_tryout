<?php

namespace App\Notifications;

use ReflectionClass;
use Illuminate\Notifications\Notification;

class DatabaseNotificationChannel 
{

    /**
     * Use the existing database notification driver
     * with that access the notifications relationship
     * that return a database model
     * 
     * ReflectionClass returns information about a class
     */
    public function send($notifiable, Notification $notification)
    {
        $data = $notification->toArray($notifiable);
        
        return $notifiable->routeNotificationFor('database')->create([
            'id' => $notification->id,
            'type' => (new ReflectionClass($notification))->getShortName(),
            'data' => $data,
            'read_at' => null,
        ]);
    }
}