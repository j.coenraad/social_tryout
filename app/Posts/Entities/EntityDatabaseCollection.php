<?php

namespace App\Posts\Entities;

use App\User;
use Illuminate\Database\Eloquent\Collection;

class EntityDatabaseCollection extends Collection
{
    public function users()
    {
        // check if the user's username matches the text in the body_plain field of the entities table
        return User::whereIn('username', $this->pluck('body_plain'))->get();
    }
}
