<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;

class ProfileCollection extends ResourceCollection
{
    public $collects = ProfileResource::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return ['data' => $this->collection];
    }

    public function with($request)
    {
        return [
            'meta' => [
                'followers' => $this->followers($request)
            ]
        ];
    }

    protected function followers($request)
    {
        if(!Auth::user()) {
            return [];
        }

        return Auth::user()->follows->pluck('id')->toArray();
    }
}
