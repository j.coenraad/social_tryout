<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'profile_image' => $this->profile_image,
            'banner_image' => $this->banner_image,
            'joined_at' => $this->created_at->format('F, Y'),
            'following_count' => $this->user->follows->count(), // profiles this user is following
            'followers_count' => $this->user->followers->count(), // profiles that follow this user
            'user' => new UserResource($this->user),
        ];
    }

    public function with($request)
    {
        return [
            'meta' => [
                'followers' => $this->followers($request)
            ]
        ];
    }

    protected function followers($request)
    {
        if(!Auth::user()) {
            return [];
        }

        return Auth::user()->follows->pluck('id')->toArray();
    }
}
