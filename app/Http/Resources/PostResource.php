<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'body' => $this->body,
            'type' => $this->type,
            'parent_id' => $this->parent_id,
            'parent_ids' => $this->parents()->pluck('id')->toArray(),
            'likes_count' => $this->likes->count(),
            'repost_count' => $this->reposts->count(),
            'replies_count' => $this->replies->count(),
            'original_post' => new PostResource($this->originalPost),
            'user' => new UserResource($this->user),
            'media' =>  new MediaCollection($this->media),
            'entities' => new EntityCollection($this->entities),
            'posted_at' => $this->created_at->format('d M. Y'),
            'created_at' => $this->created_at->timestamp,
        ];
    }
}
