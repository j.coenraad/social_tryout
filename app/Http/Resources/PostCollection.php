<?php

namespace App\Http\Resources;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PostCollection extends ResourceCollection
{
    public $collects = PostResource::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
        ];
    }

    public function with($request)
    {
        return [
            'meta' => [
                'likes' => $this->likes($request),
                'reposts' => $this->reposts($request)
            ]
        ];
    }

    protected function likes($request)
    {
        if(!$user = $request->user()) {
            return [];
        }

        // return an array of the id of the posts the authenticated user has liked
        return $user->likes()
            ->whereIn('post_id', 
                $this->collection->pluck('id')->merge($this->collection->pluck('original_post_id'))
            )
            ->pluck('post_id')
            ->toArray();
    }

    protected function reposts($request)
    {
        if(!$user = $request->user()) {
            return [];
        }

        // return an array of the id of the posts the authenticated user has shared
        return $user->reposts()
            ->whereIn('original_post_id', 
                $this->collection->pluck('id')->merge($this->collection->pluck('original_post_id'))
            )
            ->pluck('original_post_id')
            ->toArray();
    }
}
