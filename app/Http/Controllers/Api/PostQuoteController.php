<?php

namespace App\Http\Controllers\Api;

use App\Post;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use Illuminate\Http\Request;

class PostQuoteController extends Controller
{
    public function store(Request $request, Post $post)
    {
        $quote = $request->user()->posts()->create([
            'type' => 'quote',
            'body' => $request->body,
            'original_post_id' => $post->id,
        ]);

        return new PostResource($quote);
    }
}
