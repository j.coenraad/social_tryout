<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Resources\ProfileResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum'])->except(['show']);
    }

    public function show(User $user)
    {
        return new ProfileResource($user->profile);
    }

    public function update(User $user)
    {
        $profile = $user->profile;
        $profile->update(request()->validate(['description' => 'nullable|max:160']));
        return new ProfileResource($user->profile);
    }
}
