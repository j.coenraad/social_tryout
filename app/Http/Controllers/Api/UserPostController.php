<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Post;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostCollection;

class UserPostController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(User $user)
    {
        $posts = Post::where('user_id', $user->id)
            ->where('parent_id', null)
            ->with([
                'user',
                'likes', 
                'reposts', 
                'replies',
                'entities',
                'media.baseMedia',
                'originalPost.user',
                'originalPost.likes', 
                'originalPost.reposts', 
                'originalPost.media.baseMedia'
            ])->latest()->get();

        return new PostCollection($posts);
    }
}
