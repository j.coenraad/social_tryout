<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Resources\ProfileResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FollowController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function store(User $user)
    {
        auth()->user()->follow($user);

        return new ProfileResource($user->profile);
    }

    public function destroy(User $user)
    {
        auth()->user()->unfollow($user);
        
        return new ProfileResource($user->profile);
    }
}
