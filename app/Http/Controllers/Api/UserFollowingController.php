<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Profile;
use App\Http\Resources\ProfileCollection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserFollowingController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index(User $user)
    {
        $following = $user->follows->pluck('id');
        $profiles = Profile::whereIn('user_id', $following)->with('user')->paginate(10);
        return new ProfileCollection($profiles);
    }
}
