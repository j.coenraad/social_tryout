<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Media\MediaTypes;
use Illuminate\Http\Request;

class MediaTypesController extends Controller
{
    public function index()
    {
        return response()->json([
            'data' => [
                'image' => MediaTypes::$image, 
            ]
        ]);
    }
}
