<?php

namespace App\Http\Controllers\Api;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Http\Resources\PostCollection;
use App\Notifications\Posts\PostRepliedTo;

class PostReplyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum'])->only(['store']);
    }

    public function show(Post $post)
    {
        return new PostCollection($post->replies);
    }

    public function store(Post $post, Request $request)
    {
        $reply = $request->user()->posts()->create(array_merge($request->only('body'), [
            'type' => 'post',
            'parent_id' => $post->id,
        ]));

        // Only create a notification when the current user is not the owner of the post
        if($request->user()->id !== $post->user_id) {
            $post->user->notify(new PostRepliedTo($request->user(), $reply));
        }

        return new PostResource($post);
    }
}
