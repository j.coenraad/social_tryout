<?php

namespace App\Http\Controllers\Api;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Notifications\Posts\PostLiked;

class PostLikeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function store(Post $post, Request $request)
    {
        if($request->user()->hasLiked($post)){
            return response(null, 409);
        }

        $request->user()->likes()->create([
            'post_id' => $post->id
        ]);

        // Only create a notification when the current user is not the owner of the post
        if($request->user()->id !== $post->user_id){
            $post->user->notify(new PostLiked($request->user(), $post));
        }


        return new PostResource($post);
    }

    public function destroy(Post $post, Request $request)
    {
        $request->user()->likes->where('post_id', $post->id)->first()->delete();

        return new PostResource($post);
    }
}
