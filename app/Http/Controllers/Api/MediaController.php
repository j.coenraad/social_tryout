<?php

namespace App\Http\Controllers\Api;

use App\PostMedia;
use App\Http\Resources\PostMediaCollection;
use App\Media\MediaTypes;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'media.*' =>'required|mimetypes:' . implode(',', MediaTypes::$image)
        ]);

        $result = collect($request->media)->map(function ($media) {
            return $this->addMedia($media);
        });

        return new PostMediaCollection($result);
    }

    protected function addMedia($media)
    {
        $postMedia = PostMedia::create([]);

        // create a Media record
        // create a PostMedia record with the media_id referencing the media record
        $postMedia->baseMedia()
        ->associate($postMedia->addMedia($media)->toMediaCollection())
        ->save();

        return $postMedia;
    }
}
