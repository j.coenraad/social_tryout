<?php

namespace App\Http\Controllers\Api;

use App\Post;
use App\PostMedia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Http\Resources\PostCollection;
use App\Http\Requests\StorePostRequest;
use App\Notifications\Posts\PostMentionedIn;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum'])->only(['store']);
    }
    
    public function index(Request $request)
    {
        $posts = Post::with(['user',
            'likes', 
            'reposts', 
            'replies',
            'media.baseMedia',
            'originalPost.user',
            'originalPost.likes', 
            'originalPost.reposts', 
            'originalPost.media.baseMedia'
        ])->find(explode(',', $request->ids));

        return new PostCollection($posts);
    }

    public function store(StorePostRequest $request)
    {
        $post = $request->user()->posts()->create(array_merge($request->only('body'), [
            'type' => 'post'
        ]));

        foreach($request->media as $id) {
            $post->media()->save(PostMedia::find($id));
        }

        foreach($post->mentions->users() as $user){
            if($request->user()->id !== $user->id){
                $user->notify(new PostMentionedIn($request->user(), $post));
            }
        }

        return new PostResource($post);
    }

    public function show(Post $post)
    {
        $post = collect([$post])->merge($post->parents());
        return new PostCollection($post);
    }
}
