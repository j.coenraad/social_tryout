<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Profile;
use App\Http\Resources\ProfileCollection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class UserFollowerController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }

    public function index(User $user)
    {
        // get the profiles of the users that the $request->user follows
        $followers = $user->followers->pluck('id');
        $profiles = Profile::whereIn('user_id', $followers)->with(['user.follows'])->paginate(10);
        return new ProfileCollection($profiles);
    }
}
