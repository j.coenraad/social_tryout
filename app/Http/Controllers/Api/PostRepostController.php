<?php

namespace App\Http\Controllers\Api;

use App\Post;
use App\Http\Resources\PostResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostRepostController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum']);
    }
    
    public function store(Post $post, Request $request)
    {
        $repost = $request->user()->posts()->create([
            'type' => 'repost',
            'original_post_id' => $post->id,
        ]);

        return new PostResource($repost);
    }

    public function destroy(Post $post, Request $request)
    {
        $repost = $post->sharedPost;
        $post->sharedPost()->where('user_id', $request->user()->id)->delete();

        return new PostResource($repost);
    }
}
