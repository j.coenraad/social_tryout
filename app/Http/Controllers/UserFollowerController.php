<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserFollowerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(User $user)
    {
        return view('profiles.followers', compact('user'));
    }
}
