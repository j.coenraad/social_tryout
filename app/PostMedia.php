<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Media\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class PostMedia extends Model implements HasMedia
{
    use HasMediaTrait;

    public function baseMedia()
    {
        return $this->belongsTo(Media::class, 'media_id');
    }
}
