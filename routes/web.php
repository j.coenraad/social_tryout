<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

// Route::get('/timeline',  'Api\timelineController@index');

Route::get('/home', 'HomeController@index')->middleware('verified')->name('home');
Route::get('/posts/{post}', 'PostController@show');

Route::get('/notifications', 'NotificationsController@index')->name('notifications');

Route::get('/profiles/{user}', 'ProfileController@show')->name('profile');
Route::get('/profiles/{user}/following', 'UserFollowingController@index');

Route::get('/api/profile/{user}/follow', 'Api\FollowController@index');
Route::get('/profiles/{user}/followers', 'UserFollowerController');
Route::get('/profiles/{user}/following', 'UserFollowingController');