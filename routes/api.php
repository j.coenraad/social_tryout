<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/timeline',  'Api\TimelineController@index');

Route::get('/posts', 'Api\PostController@index');
Route::get('/posts/{post}', 'Api\PostController@show');
Route::post('/posts', 'Api\PostController@store');

Route::post('/posts/{post}/likes', 'Api\PostLikeController@store');
Route::delete('/posts/{post}/likes', 'Api\PostLikeController@destroy');

Route::get('/posts/{post}/replies', 'Api\PostReplyController@show');
Route::post('/posts/{post}/replies', 'Api\PostReplyController@store');

Route::post('/posts/{post}/reposts', 'Api\PostRepostController@store');
Route::delete('/posts/{post}/reposts', 'Api\PostRepostController@destroy');

Route::post('/posts/{post}/quotes', 'Api\PostQuoteController@store');

Route::post('/media','Api\MediaController@store');
Route::get('/media/types', 'Api\MediaTypesController@index');

Route::get('/notifications', 'Api\NotificationController@index');

Route::get('/profiles/{user}', 'Api\ProfileController@show');
Route::get('/users/{user}/posts', 'Api\UserPostController');

Route::put('/profiles/{user}/edit', 'Api\ProfileController@update');

Route::post('/profiles/{user}/follow', 'Api\FollowController@store');
Route::delete('/profiles/{user}/unfollow', 'Api\FollowController@destroy');

Route::get('/profiles/{user}/followers', 'Api\UserFollowerController@index');
Route::get('/profiles/{user}/following', 'Api\UserFollowingController@index');
