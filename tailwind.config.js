const plugin = require('tailwindcss/plugin')
module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      spacing: {
        '25': '6.25rem',
      },

      height: {
        'h-50': '12.5 rem'
      },

      maxHeight: {
        'max-h-50': '12.5 rem'
      }
    },
  },
  variants: {
    textColor: ['responsive', 'hover', 'focus', 'important'],
    backgroundColor: ['responsive', 'hover', 'focus', 'important'],
    borderWidth: ['responsive', 'important'],
    borderColor: ['important'],
  },
  plugins: [
    plugin(function({ addVariant }) {
      addVariant('important', ({ container }) => {
        container.walkRules(rule => {
          rule.selector = `.\\!${rule.selector.slice(1)}`
          rule.walkDecls(decl => {
            decl.important = true
          })
        })
      })
    })
  ],
}
